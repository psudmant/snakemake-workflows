# vim: syntax=python tabstop=4 expandtab
# coding: utf-8


"""
mapping to a junction database

Expects a json config file with the following structure, assuming that the
desired reference sequence is some genome
to be found under the given path, and two units A and B have been sequenced with Illumina.

{
    "junction_db_path": "/home/psudmant/projects/NAGNAG_micro_exons/ss_DB/NAGNNAG",
    "reference_index_path": "/net/uorf/data/psudmant/genomes/UCSC/UCSC",
    "symlink_dir": "/net/uorf/data/psudmant/NAGNAG/mapping/NAGNAG",
    "sequence_dir": "/net/uorf/data/psudmant/sequence/Merkin",
    "strand": "fr-firststrand",
    "species": ["macaca_mulatta","rattus_norvegicus","mus_musculus","bos_taurus","gallus_gallus"],
    "samples": ["brain","colon", "heart", "kidney", "liver", "lunk", "skm", "spleen", "testes", "brain"],
    "references_by_species" : { "gallus_gallus": "galGal3",
                                "bos_taurus":    "bosTau4",
                                "homo_sapiens" : "hg19",
                                "mus_musculus" : "mm9",
                                "rattus_norvegicus" : "rn4",
                                "macaca_mulatta": "rheMac2"
                              }
}
Note the separation between samples and units that allows to have more than
one sequencing run for each sample, or multiple lanes per sample.
"""

__author__ = "Peter Sudmant"
__license__ = "MIT"

configfile: "config.json"

#include:
#    "/home/psudmant/code/snakemake-workflows/bio/ngs/rules/transcript_analysis/tophat.rules"

rule all:
    input:
        expand("junction_entropy/{mapper}/{species}/entropies.summary", species=config["species"], mapper=config['mappers']),
        expand("NAGNAGs/{mapper}/{species}/NAGNAG_juncs.tsv", species=config["species"], mapper=config['mappers']),
        expand("NAGNAGs/{mapper}/{species}/NAGNAG.info.tsv", species=config["species"], mapper=config['mappers']),
        lambda wildcards: ["junction_entropy/%s/%s/%s.entropies"%(ma,sp,sa) for ma in config['mappers'] for sp in config["species"] for sa in config["species_to_samples"][sp] ]
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"

rule clean:
    shell:
        "rm -rf ./junction_entropy/*/*summary*; rm -rf ./NAGNAGs/*"

rule NAGNAG_info:
    input:
        "junction_entropy/{mapper}/{species}/filtered_junctions.tsv"
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"
    output:
        "NAGNAGs/{mapper}/{species}/NAGNAG_juncs.tsv",
        "NAGNAGs/{mapper}/{species}/NAGNAG.info.tsv"
    shell:
        "python ~/code/seqlib/seqlib/splicelib/get_NAGNAGs.py --fn_out_juncs {output[0]} --fn_out_info {output[1]} --fn_input_juncs {input[0]}"
        

rule summarize_filter_junctions:
    input:
        lambda wildcards: ["junction_entropy/"
                           "{mapper}/{species}/"
                           "{sample}.entropies".format(mapper=wildcards.mapper, 
                                                       species=wildcards.species, 
                                                       sample = s) 
                                                       for s in config["species_to_samples"][wildcards.species]]
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"
    output:
        "junction_entropy/{mapper}/{species}/entropies.summary",
        "junction_entropy/{mapper}/{species}/filtered_junctions.tsv",

    shell:
        "python ~/code/seqlib/seqlib/splicelib/junction_summary.py --fn_out {output[0]} --fn_inputs {input} --fn_filtered_juncs {output[1]}"
    
rule tabix_juncs:
    input:
        lambda wildcards: "{junction_db_path}/{species}/junctions/"
                            "{junction_type}.juncs".format(junction_db_path=config['junction_db_path'],
                                                           species=wildcards.species,
                                                           junction_type=config['junction_type'])
    output:
        "%s/{species}/junctions/%s.sorted.juncs.gz"%(config['junction_db_path'],config['junction_type'])
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"
    shell:
        "cat {input[0]} | sort -k 1,1 -k 2n,2n -k 3n,3n | uniq | bgzip -c >{output[0]}; tabix -p bed {output[0]}"

rule index_tophat_bam:
    input:
        lambda wildcards: "{mapping_path}/{species}/{sample}/"
                            "accepted_hits.bam".format(mapping_path=config['mapping_paths']['TOPHAT'],
                                                           species=wildcards.species,
                                                           sample=wildcards.sample)
    output:
         "%s/{species}/{sample}/accepted_hits.bam.bai"%(config['mapping_paths']['TOPHAT'])
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"

    shell:
        "samtools index {input[0]}"

rule index_STAR_bam:
    input:
        lambda wildcards: "{mapping_path}/{species}/{sample}/"
                            "Aligned.sortedByCoord.out.bam".format(mapping_path=config['mapping_paths']['STAR'],
                                                                   species=wildcards.species,
                                                                   sample=wildcards.sample)
    params:
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"
    output:
         "%s/{species}/{sample}/Aligned.sortedByCoord.out.bam.bai"%(config['mapping_paths']['STAR'])

    shell:
        "samtools index {input[0]}"

rule get_entropy:
    input:
        lambda wildcards: "{junction_db_path}/{species}/junctions/"
                            "{junction_type}.sorted.juncs.gz".format(junction_db_path=config['junction_db_path'],
                                                                     species=wildcards.species,
                                                                     junction_type=config['junction_type']),
        lambda wildcards: "{mapping_path}/{species}/{sample}/"
                            "{bam}.bai".format(mapping_path=config['mapping_paths'][wildcards.mapper],
                                                                   species=wildcards.species,
                                                                   sample=wildcards.sample,
                                                                   bam=config['bams'][wildcards.mapper])
    output: 
        "junction_entropy/{mapper}/{species}/{sample}.entropies"
    params:
        reference = lambda wildcards: config["references_by_species"][wildcards.species],
        ref_dir = config["reference_index_path"],
        junction_path = config["junction_db_path"],
        junction_type = config["junction_type"],
        mapping_path = lambda wildcards: config['mapping_paths'][wildcards.mapper],
        bam = lambda wildcards: config['bams'][wildcards.mapper],
        sge_opts="-l nodes=1:E5620:ppn=1 -q short -o logs -e logs"
    shell:
        "python ~/code/seqlib/seqlib/splicelib/junction_entropy.py --fn_fasta {params.ref_dir}/{wildcards.species}/{params.reference}/fasta/{params.reference}.fa --fn_juncs {input[0]} --fn_bam {params.mapping_path}/{wildcards.species}/{wildcards.sample}/{params.bam}  --fn_output {output[0]}"
