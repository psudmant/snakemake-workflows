import pandas as pd
import os
import json
import random
from collections import defaultdict

rule all:
    input:
        "/home/psudmant/datasets/GTEx/SraRunInfo.csv"
    output:
        "samples.json",
        "samples_subset.json",
        "samples_hi_cov_subset.json"
    run:
        T = pd.read_csv(input[0], sep=',', header=0, index_col=None)
        samples = []
        sample_subset = []
        sample_hi_cov_subset = []
        n_per_tissue_subset = 3
        
        tissue_count = defaultdict(int)
        sizes_by_sample = {}
        samples_by_tissue = {}

        for i, row in T.iterrows():
            path = row['download_path']
            path = "/".join(path.split(":")[1].split("/")[1:]).split(".")[0]
            read_1 = "%s_1.fastq.gz"%path 
            read_2 = "%s_2.fastq.gz"%path 
            sex = row['Sex']
            sex = sex[0]
            Disease = row['Disease']
            Tumor = row['Tumor']
            Htype = row['Histological_Type']
            Body_site = row['Body_Site']
            if os.path.exists(read_1):

                sym_path = "symlinked_SRP012682/{sample}" 
                
                tissue="".join([h=="-" and h or "%s%s"%(h[0].upper(),h[1:]) for h in Body_site.replace("(","").replace(")","").split(" ")])
                sample = "{tissue}_{sex}_{run}".format(run=row['Run'],
                                                       htype = Htype.replace(" ", ""), 
                                                       sex=sex,
                                                       tissue=tissue)
                sym_path = sym_path.format(sample=sample)
                reads_path="/scratch/users/psudmant/sequence/GTEx/raw_download/41738/reads"
                os.makedirs(sym_path)
                shell("ln -s {reads_path}/{read} {sym_path}".format(read=read_1,
                                                                    reads_path=reads_path,
                                                                    sym_path=sym_path))
                shell("ln -s {reads_path}/{read} {sym_path}".format(read=read_2,
                                                                    reads_path=reads_path,
                                                                    sym_path=sym_path))
                samples.append({sample:[read_1.split("/")[-1],read_2.split("/")[-1]]})
                r1_path = "{reads_path}/{read}".format(reads_path=reads_path,read=read_1)
                r2_path = "{reads_path}/{read}".format(reads_path=reads_path,read=read_2)
                sizes_by_sample[sample] = os.path.getsize(r1_path)+os.path.getsize(r2_path)
                if not tissue in samples_by_tissue:
                    samples_by_tissue[tissue] = [] 
                samples_by_tissue[tissue].append(sample)
        
        for tissue, samples in samples_by_tissue.items():
            samples = sorted(samples, key=lambda s: sizes_by_sample[s])
            sample_hi_cov_subset.append(samples[-1])
            for k in random.sample(samples, n_per_tissue_subset):
                sample_subset.append(k)

        sample_subset = {"samples":sample_subset}
        sample_hi_cov_subset = {"samples":sample_subset}

        FOUT = open(output[0],'w')
        FOUT.write(json.dumps(samples, indent=4,separators=(",", ": ")))
        FOUT = open(output[1],'w')
        FOUT.write(json.dumps(sample_subset, indent=4,separators=(",", ": ")))
        FOUT = open(output[2],'w')
        FOUT.write(json.dumps(sample_hi_cov_subset, indent=4,separators=(",", ": ")))
    
rule clean:
    shell:
        "rm -rf ./symlinked_SRP012682/*; rm samples.json"
