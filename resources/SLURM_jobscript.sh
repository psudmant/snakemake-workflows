#!/bin/sh
# properties = {properties}
curr=$$
trap 'echo parent shutting down;  child=$(ps --ppid $curr -o "%p" | head -n2 | tail -n1); echo child $child;kill -TERM -- -$child' EXIT
{exec_job}
exit 0
